from tic_tac_toc import TicTacToe
import tkinter as tk

if __name__ == '__main__':
    game = TicTacToe()
    # game.start()

    window = tk.Tk()
    window.title('test 1')
    # window.geometry('600x800')
    # lbl_1 = tk.Label(window, text='Hello World', bg='yellow', fg='#263238', font=('Arial', 12))
    # lbl_1.grid(column=0, row=0)

    # content = tk.Message(window, text='123 \n 456 \n 789', bg='yellow', fg='#263238', font=('Arial', 12))
    # content.grid(column=0, row=0)
    div_size = 200
    img_size = div_size * 2
    div1 = tk.Frame(window, width=img_size, height=img_size, bg='blue')
    div2 = tk.Frame(window, width=div_size, height=div_size, bg='orange')
    div3 = tk.Frame(window, width=div_size, height=div_size, bg='green')

    div1.grid(column=0, row=0, rowspan=2)
    div2.grid(column=1, row=0)
    div3.grid(column=1, row=1)
    window.mainloop()
