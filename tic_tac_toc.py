from enum import Enum


class State(Enum):
    Tie = 1
    Win = 2


empty = '-'


class TicTacToe:
    def __init__(self):
        self.end_state = State.Tie
        self.turn = 'O'
        self.row = 0
        self.col = 0
        self.rows = []

    def start(self):
        self.init_board()
        while True:
            self.print_board()
            if not self.column_input_valid():
                continue
            if not self.row_input_valid():
                continue
            if self.is_element_occupied():
                continue
            else:
                self.place_turn()
                self.check_end()

    def init_board(self):
        self.rows = [[empty] * 3 for _ in range(3)]

    def print_board(self):
        for i in range(3):
            for j in range(3):
                print(self.rows[i][j], end='')
            print()  # 換行

    def check_end(self):
        if self.check_win() or self.check_tie():
            self.post_process()
        else:
            self.change()

    def post_process(self):
        self.print_board()
        if self.end_state == State.Tie:
            print('平手')
        else:
            print(self.turn + '獲勝')
        self.init_board()

    def change(self):
        temp = self.turn
        self.turn = 'X' if temp == 'O' else 'O'  # 三元運算子

    def check_win(self):
        return self.is_vertical_win() or\
               self.is_horizontal_win() or\
               self.is_diagonal_win()

    # 確認左上至右下方向
    # 確認右上至左下方向
    def is_diagonal_win(self):
        result = False
        _rows = self.rows
        _turn = self.turn
        if (_rows[0][0] == _turn) \
                and (_rows[1][1] == _turn) \
                and (_rows[2][2] == _turn):
            result = True
        if (_rows[0][2] == _turn) \
                and (_rows[1][1] == _turn) \
                and (_rows[2][0] == _turn):
            result = True
        return result

    # 確認水平方向
    def is_horizontal_win(self):
        result = False
        _rows = self.rows
        _turn = self.turn
        for r in range(3):
            if (_rows[r][0] == _turn) \
                    and (_rows[r][1] == _turn) \
                    and (_rows[r][2] == _turn):
                result = True
        return result

    # 確認垂直方向
    def is_vertical_win(self):
        result = False
        _rows = self.rows
        _turn = self.turn
        for c in range(3):
            if (_rows[0][c] == _turn) \
                    and (_rows[1][c] == _turn) \
                    and (_rows[2][c] == _turn):
                result = True
        return result

    def check_tie(self):
        for r in range(3):
            for c in range(3):
                if self.rows[r][c] == empty:
                    return False
        return True

    def is_element_occupied(self):
        occupied = self.rows[self.row][self.col] != empty
        if occupied:
            print('已經輸入符號')
        return occupied

    def place_turn(self):
        self.rows[self.row][self.col] = self.turn

    def row_input_valid(self):
        try:
            self.row = int(input('列編號(0~2):'))
            if self.row in range(0, 3):
                return True
            else:
                print("Oops!  input row is not limited by 0~2")
                return False
        except ValueError:
            print("Oops!  That was no valid number.  Try again...")
            return False

    def column_input_valid(self):
        try:
            self.col = int(input('欄編號(0~2):'))
            if self.col in range(0, 3):
                return True
            else:
                print("Oops!  input column is not limited by 0~2")
                return False
        except ValueError:
            print("Oops!  That was no valid number.  Try again...")
            return False
